<?php
require_once('../connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$type = $_POST['type'];

$result[0]['name']= "Trade and investment";
$result[1]['name']= "Financial markets";
$result[2]['name']= 'Value chains';
$result[3]['name']= 'Infrastructure and connectivity';
$result[4]['name']= 'Movement of people';
$result[5]['name']= 'Regulatory cooperation';
$result[6]['name']= 'Digital economy';

$result[0]['icon']="dimension01.svg";
$result[1]['icon']= "dimension02.svg";
$result[2]['icon']='dimension03.svg';
$result[3]['icon'] = 'dimension04.svg';
$result[4]['icon'] = 'dimension05.svg';
$result[5]['icon'] = 'dimension06.svg';
$result[6]['icon'] = 'dimension07.svg';

$result[0]['color']='#64C1E8';
$result[1]['color']= '#D85B63';
$result[2]['color']='#D680AD';
$result[3]['color'] = '#88643A';
$result[4]['color'] = '#C0BA80';
$result[5]['color'] = '#FDC47D';
$result[6]['color'] = '#EA3B46';



if($type == 'Conventional'){
    $result[0]['indicator'] = ['Exports to GDP', 'Imports to GDP', 'Import tarriffs','FDI inflows to GDP', 'FDI outflows to GDP'];

    $result[1]['indicator'] = ['Cross-border portfolio to GDP', 'Deposit rates dispersion','Share price index correlation'];

    $result[2]['indicator']= ['Export complementarity index', 'Value Chains participation Index', 'Intermediate goods exports to GDP', 'Intermediate goods imports to GDP'];

    $result[3]['indicator']= ['Liner shipping connectivity index', 'Trade facilitation implement','Average internet quality','Average trade cost'];

    $result[4]['indicator']= ['Stock of emigrants per capita', 'Stock of immigrants per capita','Remittances outflow to GDP','Remittances inflow to GDP'];

    $result[5]['indicator']=['Signed FTA', 'Signed IIA (Yes/No)' , 'Embassy (yes/No)', 'Trade regulatory similarity'];

    $result[6]['indicator']=['ICT goods exports to GDP', 'ICT goods imports to GDP', 'Tariffs on ICT imports' , 'Avg. share using digital financial services', 'Avg. share of online shoppers', 'Digital Trade regulatory similarity'];
} else {
    $result[0]['indicator'] = ['Environmental goods exports to GDP', 'Environmental goods imports to GDP', 'Tariffs on environmental goods imports','Employment created by DVA'];
    $result[1]['indicator'] = ['Real exchange rate volatility', 'Avg. financial development index score','Volatility weighted correlation of share price'];
    $result[2]['indicator']= ['Environmental goods export complementarity index', 'Sustainable Value Chain participation index', 'Exports of intermediates per unit of CO2'];
    $result[3]['indicator']= ['Avg. rural access to electricity', 'Sustainable trade facilitation implementation','Avg. share of Internet users'];
    $result[4]['indicator']= ['Avg. outward remittances per immigrant','Avg. inward remittances per emigrant'];
    $result[5]['indicator']=['Sustainable FTA score', 'Sustainable IIA score' , 'Avg.  rule of law index score', 'SDG trade regulatory distance from partners'];
    $result[6]['indicator']=['Avg. secure Internet servers','Avg. share of households with Internet access', 'Avg. share of females with mobile or formal bank account', 'Avg. share of female online shoppers'];
}

echo json_encode($result);
?>