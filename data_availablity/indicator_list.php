<?php
require_once('../connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$integration=$_POST['integration'];         // "sustainable" / "conventional"
//  set $dimensionData
//  set $dimensionData
$dimensionData[0]['label']="Trade and investment";
$dimensionData[1]['label']="Financial markets";
$dimensionData[2]['label']='Value chains';
$dimensionData[3]['label']='Infrastructure and connectivity';
$dimensionData[4]['label']='Movement of people';
$dimensionData[5]['label']='Regulatory cooperation';
$dimensionData[6]['label']='Digital economy';

if($integration == 'conventional'){
    $dimensionData[0]['indicator'] = ['Exports to GDP', 'Imports to GDP', 'Import tarriffs','FDI inflows to GDP', 'FDI outflows to GDP'];

    $dimensionData[1]['indicator'] = ['Cross-border portfolio to GDP', 'Deposit rates dispersion','Share price index correlation'];

    $dimensionData[2]['indicator']= ['Export complementarity index', 'Value Chains participation Index', 'Intermediate goods exports to GDP', 'Intermediate goods imports to GDP'];

    $dimensionData[3]['indicator']= ['Liner shipping connectivity index', 'Trade facilitation implement','Average internet quality','Average trade cost'];

    $dimensionData[4]['indicator']= ['Stock of emigrants per capita', 'Stock of immigrants per capita','Remittances outflow to GDP','Remittances inflow to GDP'];

    $dimensionData[5]['indicator']=['Signed FTA', 'Signed IIA (Yes/No)' , 'Embassy (yes/No)', 'Trade regulatory similarity'];

    $dimensionData[6]['indicator']=['ICT goods exports to GDP', 'ICT goods imports to GDP', 'Tariffs on ICT imports' , 'Avg. share using digital financial services', 'Avg. share of online shoppers', 'Digital Trade regulatory similarity'];
}
else{
    $dimensionData[0]['indicator'] = ['Environmental goods exports to GDP', 'Environmental goods imports to GDP', 'Tariffs on environmental goods imports','Employment created by DVA'];
    $dimensionData[1]['indicator'] = ['Real exchange rate volatility', 'Avg. financial development index score','Volatility weighted correlation of share price'];
    $dimensionData[2]['indicator']= ['Environmental goods export complementarity index', 'Sustainable Value Chain participation index', 'Exports of intermediates per unit of CO2'];
    $dimensionData[3]['indicator']= ['Avg. rural access to electricity', 'Sustainable trade facilitation implementation','Avg. share of Internet users'];
    $dimensionData[4]['indicator']= ['Avg. outward remittances per immigrant','Avg. inward remittances per emigrant'];
    $dimensionData[5]['indicator']=['Sustainable FTA score', 'Sustainable IIA score' , 'Avg.  rule of law index score', 'SDG trade regulatory distance from partners'];
    $dimensionData[6]['indicator']=['Avg. secure Internet servers','Avg. share of households with Internet access', 'Avg. share of females with mobile or formal bank account', 'Avg. share of female online shoppers'];
}
echo json_encode($dimensionData);
?>