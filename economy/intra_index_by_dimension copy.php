<?php
require_once('../connection.php');
$_POST = json_decode(file_get_contents("php://input"),true);
$input=$_POST['input'];
$countryReportList=$_POST['countryReportList'];
$reportMap=$_POST['reportMap'];
$countryPartnerList=$_POST['countryPartnerList'];
$partnerMap=$_POST['partnerMap'];

$yearMax = $input['year']['max'];
$yearMin = $input['year']['min'];

$type=$input['type'];

// $type = "Sustainable";
// $yearMin = 2010;
// $yearMax = 2019;
// $partnerMap = ["AUS","THA"];
// $reportMap = ["CHN"];




if($type == "Sustainable"){
    $table = "ri_intra_sus_alldim";
} else {
    $table = "ri_intra_con_alldim";
}




// $reportTemp = "(";
// for($i=0; $i<sizeof($reportMap);$i++){
//     $reportTemp .= "'" . $reportMap[$i] . "', ";
// }
// $reportTemp = rtrim($reportTemp," ,") . ")";
// if(sizeof($partnerMap) > 1){
// $partnerTemp = "(";
// for($i=0; $i<sizeof($partnerMap);$i++){
//     $partnerTemp .= "'" . $partnerMap[$i] . "', ";
// }
// $partnerTemp = rtrim($partnerTemp," ,") . ")";
// } else{
//     $partnerTemp = "('" . $partnerMap . "')";
// }

// $sql = "select dimension, avg(score) as score , year from " . $table . " where reporter in " . $reportTemp . " and partner in " . $partnerTemp . " and (year between " . $yearMin . " and " . $yearMax .  ") group by dimension, year";
// $result = $db->query($sql)->fetchAll();

$result =$db->select($table,[
    "dimension",
    "score",
    "year"
],[
    "reporter"=>$reportMap,
    "partner"=>$partnerMap,
    "year[<>]"=>[$yearMin,$yearMax]
]);


echo json_encode($result);
?>